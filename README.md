# Encuesta de encuestas

Proyecto para publicar información relacionada a las encuestas generadas por Nacion321, a través de una API RestFul

### Resumen ###

La información se encuentra en una base de datos mysql, y se publica a través de el framework Hapi.js.

Por medio de diferentes plugins se complementa la siguiente funcionalidad: 

* Para reportar una bitácora en consola de acuerdo a la actividad registrada
* Publicar y consumir los "endpoints" de la API
* Acceder a mysql por medio de un query builder

### Version ###

* 1.0.0 


### Resumen de instalación ###

```npm init```

```npm install --save hapi```

```npm install hapi-swagger --save```

```npm install inert --save```

```npm install vision --save```

```npm i --save knex```

```npm i --save mysql```

```npm install --save good```

```npm install --save good-console```

```npm install --save H2o2```

```npm install --save babel-cli babel-preset-node6```

```npm install --save socket.io```

### Configuración ###
Habilitar el puerto tcp 9000
### Dependencias ###

Se habilitan los siguientes plugins:
* hapi.- Framework sobre el cual se construye el aplicativo
* swagger.- Plugin para documentar, publicar y consumir la información correspondiente.
* inert (sirve archivos staticos como js, css)
* vision(para ver vistas, valga la redundancia) van implicitos con swagger.
* Knex es un query builder para entrar a mysql desde hapi. Y usa mysql, de lo contrario marca un error, hay que instalarlo
* good y good-console son para la funcionalidad de la bitacora
* H2o2 plugina permite habilitar la funcionalidad como proxy, ejecuta una url externa
* babel-cli babel-preset-node6 para habilitar ECMA 6
* socket.io habilita el envio de mensajes en tiempo real

### Configuración para acceder a la Base de Datos ###

La configuración de acceso a la base de datos se encuentra en el archivo src/knex.js
### Cómo ejecutar las pruebas e iniciar el aplicativo ###

```npm start```

### Detalles de ejecución ###

Al iniciar el proyecto, en consola se reportan las rutas habilitadas con su correspondiente verbo.

La ejecución del método para obtener info de una Api Externa considera el caché configurado en el header de la respuesta remota.
