var Knex = require('./knex');
var Wreck = require('wreck');

const funciones = {};

funciones.hello = (request, reply) => {
    reply('Hello World!');
};

funciones.ligaExterna = (request, callback) => {

        //Asignando el header de Autorizacion encriptado a Bloomberg

        //const headers = {'Authorization': 'Basic ' + new Buffer(API_USERNAME+':'+ API_PASSWORD).toString('base64') };


        console.log('Antes de invocar las notas de ef');
        callback(null, 'http://api.nacion321.com/article-last-ef/');
    }

funciones.ligaExternaRespuesta =  (err, res, request, reply, settings, ttl) => {

    console.log('receiving the response from the upstream.');
    Wreck.read(res, {json: true}, function (err, payload) {

        console.log('Se mostraron Currencies.')
        reply(payload).headers = res.headers;
    });
}

funciones.listCandidatos = (request, reply) => {

    // In general, the Knex operation is like Knex('TABLE_NAME').where(...).chainable(...).then(...)
    const getOperation = Knex('ee_candidato').where({

        //isPublic: false

    }).select('*').then((results) => {

        if (!results || results.length === 0) {

            reply({

                error: true,
                errMessage: 'no candidate found',

            });

        }

        reply({

            dataCount: results.length,
            data: results,

        });

    }).catch((err) => {

        reply('server-side error');

    });


};

funciones.listEncuestadoras = (request, reply) => {

    // In general, the Knex operation is like Knex('TABLE_NAME').where(...).chainable(...).then(...)
    const getOperation = Knex('ee_encuestadora').where({

        //isPublic: false

    }).select('encuestadora').then((results) => {
        if (!results || results.length === 0) {
            reply({
                error: true,
                errMessage: 'no candidate found',
            });
        }
        reply({
            dataCount: results.length,
            data: results,
        });
    }).catch((err) => {
        reply('server-side error');
    });
};

module.exports = funciones;
