/**
 * @author Luis Zenteno
 * @created 4/6/17
 */

const homeAdmin = {
    method : 'GET',
    path : '/admin/home',
    handler : (request, reply) => {
        reply.view('admin/home');
    }
};

const adminRoutes = [
    homeAdmin
];

export default adminRoutes;