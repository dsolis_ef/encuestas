var funciones = require('./../funciones');


const hello ={
    method: 'GET',
        path: '/hello',
    config: {
    handler: funciones.hello,
        description: 'Funcion demostrativa simplemente',
        notes: 'Returns una cadena que evidencia la habilitacion de una ruta',
        tags: ['api', 'demo']
}

};

const getCandidatos = {
    path: '/candidatos',
    method: 'GET',
    config: {
        handler: funciones.listCandidatos,
        description: 'Funcion para listar el catalogo de candidatos',
        notes: 'Es un método publico',
        tags: ['api']
    }
};

const getEncuestadoras = {
    path: '/encuestadoras',
    method: 'GET',
    config: {
        handler: funciones.listEncuestadoras,
        description: 'Funcion para listar el catalogo de Encuestadoras',
        notes: 'Es un método publico',
        tags: ['api']
    }
};

const apiExterna={
    method: 'GET',
        path: '/apiexterna',
        config: {
        handler: {
            proxy: {
                mapUri: funciones.ligaExterna,
                    onResponse:funciones.ligaExternaRespuesta
            }
        },
        description: 'Funcion para invocar un recurso externo',
            notes: 'Es un método publico',
            tags: ['api']
    }

};

const apiRoutes = [
    hello,
    getCandidatos,
    getEncuestadoras,
    apiExterna
];

export default apiRoutes;
