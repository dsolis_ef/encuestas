import frontRoutes from './frontRoutes';
import apiRoutes from './apiRoutes';
import adminRoutes from './admin/adminRoutes';

/**
 * Funcion que agrega todas las rutas a la lista principal de routes
 */
const addRoutes = () => {
    const routeList = [];
    // por cada "archivo" de rutas, se debe agregar como en estos ejemplos
    frontRoutes.forEach((route) => routeList.push(route));
    apiRoutes.forEach((route) => routeList.push(route));
    adminRoutes.forEach((route) => routeList.push(route));

    return routeList;
};

const routes = addRoutes();

export default routes;

