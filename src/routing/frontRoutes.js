var funciones = require('./../funciones');


const testing = {
    method: 'GET',
    path: '/testing',
    handler: function (request, reply) {
    //reply('Hello DSG!')
    reply.view('testing/prueba', {title : 'Pagina de Prueba' });
}
};

const assets = {
    method : 'GET', path : '/public/{path*}', handler : {
    directory : {
        path : './public',
            listing : false,
            index : false
    }
}
};

const home = {
    method : 'GET', path : '/', handler: function (request, reply) {
        //reply('Hello DSG!')
        reply.view('home/home', {title : 'Inicio' });
    }
};

const votonacional = {
    method : 'GET', path : '/votonacional', handler: function (request, reply) {
        //reply('Hello DSG!')
        reply.view('home/votonacional', {title : 'Voto Nacional' });
    }
};

const graph = {
    method: 'GET',
        path: '/graph',
    handler: function (request, reply) {
    //reply('Hello DSG!')
    reply.view('graph/index', {title : 'Votaciones RT' });
}
};


const frontRoutes = [
    testing,
    assets,
    home,
    votonacional,
    graph
];

export default frontRoutes;