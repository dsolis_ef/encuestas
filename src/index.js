// Include Hapi package
var Hapi = require('hapi');
var Inert = require('inert');
var Vision = require('vision');
var Pack = require('.././package');
const Path = require('path');

//var routes = require('./routing/routes');
import routes from './routing/routes';

const server = new Hapi.Server();

server.connection( {
    host: 'localhost',
    port: 9000, routes: { log: true }
});


let io = require('socket.io')(server.listener);



const options = {
    info: {
            'title': 'API implementada en Hapi.js para publicar información de Encuestas',
            'version': Pack.version,
        }
    };

// Register Swagger Plugin ( Use for documentation and testing purpose )
server.register([
    Inert,
    Vision,
{
    register: require('hapi-swagger'),
    options: options
},
    {
        register: require('h2o2')
    },{
    register: require('good'),
    options: {
        opsInterval: 1000,
        reporters: [{
            reporter: require('good-console'),
            events: {log: '*', response: '*'}
        }]
    }
}], function (err) {
    if (err) {
        server.log(['error'], 'Error: ' + err);
        console.error(err);
    } else {
        server.log(['start'], 'No errors');
    }
});

server.views({
    engines:{
        html: require('handlebars')
    },
    layout : true,
    relativeTo: __dirname,
    path: '../views',
    layoutPath : Path.join(__dirname, '../views/layouts'), //setting Global Layout,
    partialsPath : Path.join(__dirname,'../views/layouts/partial') //partial Views
});

// =============== Routes for our API =======================
// Define GET route
 routes.forEach( ( route ) => {

            console.log( 'attaching:' + route.path + " with " + route.method );
            server.route( route );
        } );



// Lets start the server
server.start(function () {
    console.log('Server running at:', server.info.uri);
});



let mes_actual=0;

setInterval(function() {


    let valor= (1) * Math.round(Math.random() * 100);

    let valor2= (1) * Math.round(Math.random() * 100);

    let valor3= (1) * Math.round(Math.random() * 100);

    let valor4= (1) * Math.round(Math.random() * 100);

    let mes = mes_actual%12;

    io.sockets.emit('timer', {  valor: valor, mes: mes, valor2: valor2 , valor3: valor3 , valor4: valor4 });

    mes_actual++;

}, 1000);




io.on('connection',function(socket){
    console.log(socket.id);
    socket.emit('connected', { count: 'Ready'});

});
